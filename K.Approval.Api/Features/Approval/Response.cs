using K.Approval.Api.Common;

namespace K.Approval.Api.Features.Approval
{
    public class Response : BaseResponse
    {
        public string DisplayStatus { get; set; }
        public string NewStatus { get; set; }
        public string RequestNumber { get; set; }
    }
}