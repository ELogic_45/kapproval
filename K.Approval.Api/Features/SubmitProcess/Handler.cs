using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using KCore.Common.Fault;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Features.SubmitProcess
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public Handler(IHttpContextAccessor httpContextAccessor, WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
            _httpContextAccessor = httpContextAccessor;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var approvalHelper = new ApprovalHelper(_workflowDataContext);
            var requestParameter = Request.ToRequestParameter(request);
            var activeRequest = await approvalHelper.BaseNewProcess(requestParameter);

            var now = DateTime.Now;

            // get first activity process
            var processActivity = await _workflowDataContext.ProcessActivities.Where(a => a.ProcessRequestId == activeRequest.Id && a.RowStatus == 0)
                    .OrderBy(c => c.ActivityIndex)
                    .FirstOrDefaultAsync(cancellationToken);

            // validate data activity process
            if (processActivity == null) throw new ApiException(ProcessError.InvalidConfigurationProcessActivity);
            if (processActivity.ActivityIndex != 1) throw new ApiException(ProcessError.InvalidConfigurationProcessActivityIndex);

            // validate activity actor
            var processActivityActors = await _workflowDataContext.ProcessActivityActors.
                Where(c => c.ProcessActivityId == processActivity.Id && c.RowStatus == 0).ToListAsync(cancellationToken);

            var processActivityActor = processActivityActors.FirstOrDefault(c => c.ActorCode == request.UserProfile.EmployeeCode && 
                                                                                 c.ActionType == (byte)InboxActionType.Initiator);
            if (processActivityActor == null)
                throw new ApiException(ProcessError.InvalidProcessAuthorization(request.UserProfile.EmployeeCode));

            var requestStatus = NewRequestStatus(request, processActivity, activeRequest.RequestNumber);
            
            requestStatus.Subject = ApprovalHelper.CreatePreSubject(processActivity, requestStatus, processActivityActor, requestParameter);
            await _workflowDataContext.RequestStatuses.AddAsync(requestStatus, cancellationToken);

            var activity = NewRequestActivity(request, processActivity, requestStatus, processActivityActor, activeRequest.RequestNumber);
            await _workflowDataContext.RequestActivities.AddAsync(activity, cancellationToken);


            var emailTo = "";
            foreach (var activityActor in processActivityActors)
            {
                emailTo += activityActor.ActorEmail + ";";
                
                //if (activityActor.ActorCode == requestParameter.UserName) continue;
                
                var subject = activityActor.ActorCode == requestParameter.UserName ?
                    processActivity.PostSubject.CreateSubject(requestStatus.RequestNumber, requestStatus.ActorName, "", requestParameter.DocumentNumber) : 
                    processActivity.ViewSubject.CreateSubject(requestStatus.RequestNumber, requestStatus.ActorName, "", requestParameter.DocumentNumber);
                var requestInbox = new RequestInbox
                {
                    Id = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    CreatedBy = requestParameter.UserName,
                    RowStatus = 0,
                    ModifiedBy = string.Empty,
                    ModifiedDate = DateTime.Now,
                    CommitmentDate = null,
                    RequestDate = requestParameter.RequestDate,
                    RequestNumber = activeRequest.RequestNumber,
                    RequestStatus = processActivity.NewStatus,
                    ActorNameRequester = requestParameter.FullName,
                    Subject = subject,
                    ActivityUrl = "",
                    AssignDate = DateTime.Now,
                    CompleteDate = DateTime.Now,
                    HasView = false,
                    IsComplete = true,
                    IsDelegatee = false,
                    RequestDelegateFromId = string.Empty,
                    RequestDelegateFromName = string.Empty,
                    ActorCodeAssignee = activityActor.ActorCode,
                    ActorNameAssignee = activityActor.ActorName,
                    ActorCodeRequester = requestParameter.UserName,
                    JavascriptAction = processActivity.ViewJavascriptAction,
                    UrlAction = processActivity.UrlAction,
                    UrlActionType = processActivity.UrlActionType,
                    ProcessActivityId = processActivity.Id,
                    Description = requestParameter.Description,
                    DisplayStatus = processActivity.DisplayName,
                    ObjectId = requestParameter.ObjectKey,
                    ActionType = InboxActionType.View.ToString(),
                    DocumentName = requestParameter.DocumentName,
                    DocumentNumber = requestParameter.DocumentNumber,
                    
                };
                await _workflowDataContext.RequestInboxes.AddAsync(requestInbox, cancellationToken);
            }

            var emailTask = new EmailTask
            {
                Id = Guid.NewGuid(),
                CreatedDate = now,
                CreatedBy = requestParameter.UserName,
                RowStatus = 0,
                ModifiedBy = "",
                ModifiedDate = DateTime.Now,
                EmailBody = "",
                EmailCc = "",
                EmailFrom = "",
                EmailSubject = ApprovalHelper.CreatePreSubject(processActivity, requestStatus, processActivityActor, requestParameter),
                EmailTo = emailTo,
                SourceId = processActivity.Id,
                TaskFrom = "NEW PROCESS",
            };

            await _workflowDataContext.EmailTasks.AddAsync(emailTask, cancellationToken);
            await _workflowDataContext.SaveChangesAsync(cancellationToken);
            requestParameter.RequestNumber = activeRequest.RequestNumber;

            await approvalHelper.ApprovalProcess(processActivity, requestParameter);

            await _workflowDataContext.SaveChangesAsync(cancellationToken);

            return ApiResult<Response>.Ok(new Response
            {
                Url = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl(),
                NewStatus = requestStatus.NewRequestStatus,
                DisplayStatus = requestStatus.DisplayStatus,
                RequestNumber = request.RequestNumber
            });
        }
        
        private static RequestActivity NewRequestActivity(Request request, ProcessActivity processActivity, RequestStatus requestStatus, 
            ProcessActivityActor processActivityActor, string requestNumber)
        {
            return new RequestActivity
            {
                Description = request.Description,
                RequestStatus = processActivity.NewStatus,
                ProcessActivityId = processActivity.Id,
                ActivityIndex = processActivity.ActivityIndex,
                DisplayStatus = processActivity.DisplayName,
                ActorCode = request.UserProfile.EmployeeCode,
                ActionDate = request.RequestDate,
                ActorName = request.UserProfile.FullName,
                ActionName = processActivity.NewStatus,
                CreatedBy = request.GetAccessor(),
                CreatedDate = DateTime.UtcNow,
                Id = Guid.NewGuid(),
                IsComplete = 1,
                ModifiedBy = request.GetAccessor(),
                ModifiedDate = DateTime.UtcNow,
                RowStatus = 0,
                SlaTime = null,
                SlaType = null,
                SubjectName = ApprovalHelper.CreatePreSubject(processActivity, requestStatus, processActivityActor, Request.ToRequestParameter(request)),
                ObjectKey = request.ObjectKey,
                RequestNumber = requestNumber,
                DocumentName = request.DocumentName,
                DocumentNumber = request.DocumentNumber
            };
        }

        
        private static RequestStatus NewRequestStatus(Request request, ProcessActivity processActivity, string requestNumber)
        {
            return new RequestStatus
            {
                Id = Guid.NewGuid(),
                ProcessActivityId = processActivity.Id,
                SlaTime = processActivity.SlaTime,
                CreatedDate = DateTime.UtcNow,
                NewRequestStatus = processActivity.NewStatus,
                CreatedBy = request.GetAccessor(),
                RowStatus = 0,
                SlaType = processActivity.SlaType,
                CommitmentDate = null,
                LastAssignDate = request.RequestDate,
                LastAssignTo = request.UserProfile.FullName,
                ModifiedBy = request.GetAccessor(),
                ModifiedDate = DateTime.UtcNow,
                Notes = request.Description,
                RequestDate = request.RequestDate,
                RequestNumber = requestNumber,
                DisplayStatus = processActivity.DisplayName,
                ActorCode = request.UserProfile.EmployeeCode,
                ActorName = request.UserProfile.FullName,
                Subject = processActivity.SubjectName,
                ObjectKey = request.ObjectKey,
                IsComplete = 0,
                DocumentName = request.DocumentName,
                DocumentNumber = request.DocumentNumber
            };
        }

        
    }
}