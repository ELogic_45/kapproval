using System;
using K.Approval.Api.Common;
using KCore.Common.Response;
using MediatR;

namespace K.Approval.Api.Features.Revise
{
    public class Request : BaseRequest, IRequest<ApiResult<Response>>
    {
        public string Action { get; set; }
        public string Email { get; set; }
        public string ActionName { get; set; }
        public Guid InboxId { get; set; }
        internal string ObjectId { get; set; }
        internal string RequestNumber { get; set; }
        internal string DocumentName { get; set; }
        internal string DocumentNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public string Description { get; set; }
    }
}