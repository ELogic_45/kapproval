using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class UserSecurity
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        [StringLength(64)]
        public string UserPassword { get; set; }
        [Required]
        public bool MustChangePassword { get; set; }
        [Required]
        public byte IsAdministrator { get; set; }
        [StringLength(128)]
        public string SecurityQuestion { get; set; }
        [StringLength(128)]
        public string SecurityAnswer { get; set; }
        public DateTime? PasswordExpiredAt { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }


    }
}