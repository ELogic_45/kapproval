using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class ApplicationRegistration
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid CompanyId { get; set; }
        [StringLength(24)]
        [Required]
        public string ApplicationKey { get; set; }
        [StringLength(24)]
        [Required]
        public string ApplicationSecret { get; set; }
        [StringLength(24)]
        public string NodeCluster { get; set; }
        [StringLength(128)]
        [Required]
        public string Owner { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        
        public virtual Company Company { get; set; }
    }
}