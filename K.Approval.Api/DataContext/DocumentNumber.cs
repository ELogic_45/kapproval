using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class DocumentNumber
    {
        public DocumentNumber()
        {
            DocumentNumberPatterns = new HashSet<DocumentNumberPattern>();
        }

        public ICollection<DocumentNumberPattern> DocumentNumberPatterns { get; set; }
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        [StringLength(12)]
        public string DocumentCode { get; set; }
        [Required]
        [StringLength(64)]
        public string DocumentName { get; set; }
        [Required]
        public int DocumentPattern { get; set; }
        [Required]
        [StringLength(12)]
        public string PrefixChar { get; set; }
        [Required]
        [StringLength(12)]
        public string SuffixChar { get; set; }
        [Required]
        public int LastIndex { get; set; }
        [Required]
        public int LengthNumber { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }


    }
}