using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    using System;
    
    public class EmailTask
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [StringLength(64)]
        [Required]
        public string TaskFrom { get; set; }
        public Guid SourceId { get; set; }
        [Required]
        [StringLength(128)]
        public string EmailFrom { get; set; }
        [Required]
        [StringLength(128)]
        public string EmailTo { get; set; }
        [StringLength(1024)]
        public string EmailCc { get; set; }
        [Required]
        [StringLength(128)]
        public string EmailSubject { get; set; }
        [Required]
        public string EmailBody { get; set; }
        public int ResendCount { get; set; }
        [StringLength(128)]
        public string ErrorMessage { get; set; }
        public DateTime? StartSend { get; set; }
        public DateTime? EndSend { get; set; }
        public bool? IsSuccess { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}