﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class changeProcessRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndActive",
                table: "TProcessRequests");

            migrationBuilder.DropColumn(
                name: "EndActive",
                table: "ProcessRequests");

            migrationBuilder.RenameColumn(
                name: "StartActive",
                table: "TProcessRequests",
                newName: "EffectiveDate");

            migrationBuilder.RenameColumn(
                name: "StartActive",
                table: "ProcessRequests",
                newName: "EffectiveDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EffectiveDate",
                table: "TProcessRequests",
                newName: "StartActive");

            migrationBuilder.RenameColumn(
                name: "EffectiveDate",
                table: "ProcessRequests",
                newName: "StartActive");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndActive",
                table: "TProcessRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "EndActive",
                table: "ProcessRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
