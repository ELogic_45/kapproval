﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class updatedeui3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivity_TProcessRequest_TProcessRequestId",
                table: "TProcessActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivityActor_TProcessActivity_TProcessActivityId",
                table: "TProcessActivityActor");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessRequest_Workflows_WorkflowId",
                table: "TProcessRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessRequest",
                table: "TProcessRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivityActor",
                table: "TProcessActivityActor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivity",
                table: "TProcessActivity");

            migrationBuilder.RenameTable(
                name: "TProcessRequest",
                newName: "TProcessRequests");

            migrationBuilder.RenameTable(
                name: "TProcessActivityActor",
                newName: "TProcessActivityActors");

            migrationBuilder.RenameTable(
                name: "TProcessActivity",
                newName: "TProcessActivitys");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessRequest_CompanyId_ModuleId",
                table: "TProcessRequests",
                newName: "IX_TProcessRequests_CompanyId_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessRequest_WorkflowId",
                table: "TProcessRequests",
                newName: "IX_TProcessRequests_WorkflowId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivityActor_TProcessActivityId",
                table: "TProcessActivityActors",
                newName: "IX_TProcessActivityActors_TProcessActivityId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivity_TProcessRequestId",
                table: "TProcessActivitys",
                newName: "IX_TProcessActivitys_TProcessRequestId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessRequests",
                table: "TProcessRequests",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivityActors",
                table: "TProcessActivityActors",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivitys",
                table: "TProcessActivitys",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivitys_TProcessActivityId",
                table: "TProcessActivityActors",
                column: "TProcessActivityId",
                principalTable: "TProcessActivitys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivitys_TProcessRequests_TProcessRequestId",
                table: "TProcessActivitys",
                column: "TProcessRequestId",
                principalTable: "TProcessRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessRequests_Workflows_WorkflowId",
                table: "TProcessRequests",
                column: "WorkflowId",
                principalTable: "Workflows",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivitys_TProcessActivityId",
                table: "TProcessActivityActors");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivitys_TProcessRequests_TProcessRequestId",
                table: "TProcessActivitys");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessRequests_Workflows_WorkflowId",
                table: "TProcessRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessRequests",
                table: "TProcessRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivitys",
                table: "TProcessActivitys");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivityActors",
                table: "TProcessActivityActors");

            migrationBuilder.RenameTable(
                name: "TProcessRequests",
                newName: "TProcessRequest");

            migrationBuilder.RenameTable(
                name: "TProcessActivitys",
                newName: "TProcessActivity");

            migrationBuilder.RenameTable(
                name: "TProcessActivityActors",
                newName: "TProcessActivityActor");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessRequests_CompanyId_ModuleId",
                table: "TProcessRequest",
                newName: "IX_TProcessRequest_CompanyId_ModuleId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessRequests_WorkflowId",
                table: "TProcessRequest",
                newName: "IX_TProcessRequest_WorkflowId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivitys_TProcessRequestId",
                table: "TProcessActivity",
                newName: "IX_TProcessActivity_TProcessRequestId");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivityActors_TProcessActivityId",
                table: "TProcessActivityActor",
                newName: "IX_TProcessActivityActor_TProcessActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessRequest",
                table: "TProcessRequest",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivity",
                table: "TProcessActivity",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivityActor",
                table: "TProcessActivityActor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivity_TProcessRequest_TProcessRequestId",
                table: "TProcessActivity",
                column: "TProcessRequestId",
                principalTable: "TProcessRequest",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivityActor_TProcessActivity_TProcessActivityId",
                table: "TProcessActivityActor",
                column: "TProcessActivityId",
                principalTable: "TProcessActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessRequest_Workflows_WorkflowId",
                table: "TProcessRequest",
                column: "WorkflowId",
                principalTable: "Workflows",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
