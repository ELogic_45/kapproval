﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class changeentityyname1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 128, nullable: false),
                    LegalName = table.Column<string>(maxLength: 128, nullable: false),
                    BusinessPhone = table.Column<string>(maxLength: 24, nullable: true),
                    BusinessFax = table.Column<string>(maxLength: 24, nullable: true),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Url = table.Column<string>(maxLength: 128, nullable: true),
                    Npwp = table.Column<string>(maxLength: 32, nullable: true),
                    Address1 = table.Column<string>(maxLength: 64, nullable: true),
                    Address2 = table.Column<string>(maxLength: 64, nullable: true),
                    ZipCode = table.Column<string>(maxLength: 6, nullable: true),
                    City = table.Column<string>(maxLength: 64, nullable: true),
                    State = table.Column<string>(maxLength: 64, nullable: true),
                    Country = table.Column<string>(maxLength: 64, nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserImages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ImageProfile = table.Column<byte[]>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserImages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserSecurities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    UserPassword = table.Column<string>(maxLength: 64, nullable: false),
                    MustChangePassword = table.Column<bool>(nullable: false),
                    IsAdministrator = table.Column<byte>(nullable: false),
                    SecurityQuestion = table.Column<string>(maxLength: 128, nullable: true),
                    SecurityAnswer = table.Column<string>(maxLength: 128, nullable: true),
                    PasswordExpiredAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSecurities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProfiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    EmployeeCode = table.Column<string>(maxLength: 24, nullable: false),
                    FullName = table.Column<string>(maxLength: 64, nullable: false),
                    UserName = table.Column<string>(maxLength: 24, nullable: false),
                    DeviceId = table.Column<string>(maxLength: 32, nullable: true),
                    Email = table.Column<string>(maxLength: 128, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    UserImageId = table.Column<Guid>(nullable: true),
                    UserSecurityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserProfiles_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserProfiles_UserImages_UserImageId",
                        column: x => x.UserImageId,
                        principalTable: "UserImages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserProfiles_UserSecurities_UserSecurityId",
                        column: x => x.UserSecurityId,
                        principalTable: "UserSecurities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    UserProfileId = table.Column<Guid>(nullable: false),
                    Action = table.Column<string>(maxLength: 24, nullable: true),
                    LogDate = table.Column<DateTime>(nullable: false),
                    ApplicationName = table.Column<string>(maxLength: 32, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserLogs_UserProfiles_UserProfileId",
                        column: x => x.UserProfileId,
                        principalTable: "UserProfiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserLogs_UserProfileId",
                table: "UserLogs",
                column: "UserProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfiles_CompanyId",
                table: "UserProfiles",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfiles_UserImageId",
                table: "UserProfiles",
                column: "UserImageId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfiles_UserSecurityId",
                table: "UserProfiles",
                column: "UserSecurityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserLogs");

            migrationBuilder.DropTable(
                name: "UserProfiles");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "UserImages");

            migrationBuilder.DropTable(
                name: "UserSecurities");
        }
    }
}
