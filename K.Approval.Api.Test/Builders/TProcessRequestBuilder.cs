using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class TProcessRequestBuilder : BaseFluentBuilder<TProcessRequest>
    {
        public TProcessRequestBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public TProcessRequestBuilder WithCompany(string data)
        {
            SetProperty(x => x.CompanyId, data);
            return this;
        }
        
        public TProcessRequestBuilder WithModuleId(string data)
        {
            SetProperty(x => x.ModuleId, data);
            return this;
        }
        public TProcessRequestBuilder WithEffectiveDate(DateTime data)
        {
            SetProperty(x => x.EffectiveDate, data);
            return this;
        }
        
        public TProcessRequestBuilder WithWorkflow(Guid data)
        {
            SetProperty(x => x.WorkflowId, data);
            return this;
        }
        
    }
}