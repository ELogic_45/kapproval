using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class DocumentNumberPatternBuilder : BaseFluentBuilder<DocumentNumberPattern>
    {
        public DocumentNumberPatternBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
            SetProperty(x => x.ResetMonthly, false);
            SetProperty(x => x.ResetAnnually, false);
        }
        
        public DocumentNumberPatternBuilder WithDocumentCode(Guid data)
        {
            SetProperty(x => x.DocumentNumberId, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithDocumentPatternType(DocumentPatternType data)
        {
            SetProperty(x => x.PatterType, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithLengthNumber(int data)
        {
            SetProperty(x => x.LengthNumber, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithSeparator(string data)
        {
            SetProperty(x => x.Separator, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithResetAnnually(bool data)
        {
            SetProperty(x => x.ResetAnnually, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithResetMonthly(bool data)
        {
            SetProperty(x => x.ResetMonthly, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithFixedValue(string data)
        {
            SetProperty(x => x.FixedValue, data);
            return this;
        }
        
        public DocumentNumberPatternBuilder WithLastIndex(int data)
        {
            SetProperty(x => x.LastIndex, data);
            return this;
        }
        
    }
}