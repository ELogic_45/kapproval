using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class TProcessActivityBuilder: BaseFluentBuilder<TProcessActivity>
    {
        public TProcessActivityBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }

        public TProcessActivityBuilder WithProcessRequest(Guid data)
        {
            SetProperty(x => x.ProcessRequestId, data);
            return this;
        }
        
        public TProcessActivityBuilder WithActivityIndex(int data)
        {
            SetProperty(x => x.ActivityIndex, data);
            return this;
        }
        
        public TProcessActivityBuilder WithNewStatus(string newStatus, string displayName)
        {
            SetProperty(x => x.NewStatus, newStatus);
            SetProperty(x => x.DisplayName, displayName);
            return this;
        }
        
        public TProcessActivityBuilder WithPostSubject(string data)
        {
            SetProperty(x => x.PostSubject, data);
            return this;
        }
        
        public TProcessActivityBuilder WithViewSubject(string data)
        {
            SetProperty(x => x.ViewSubject, data);
            return this;
        }
        
        public TProcessActivityBuilder WithSubjectName(string data)
        {
            SetProperty(x => x.SubjectName, data);
            return this;
        }
    }
}