using System;
using System.Linq;
using KCore.Common.Data;
using Shouldly;
using Xunit;

namespace K.Approval.Api.Test.Common
{
    public class LinqDynamicHelperTest
    {
        [Fact]
        public void EqualAndNotEqualStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column"), WhereTerm.Default("value2", "column2", SqlOperator.NotEqual)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe(" (column.Equals(@0))  AND  (column2.Equals(@1) == false)  ");
        }
        
        [Fact]
        public void MultipleEqualStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column"), WhereTerm.Default("value2", "column2", SqlOperator.Equals)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe(" (column.Equals(@0))  AND  (column2.Equals(@1))  ");
        }
        
        [Fact]
        public void MultipleEqualWithOrStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column", SqlOperator.Equals, LogicalOperator.OR), WhereTerm.Default("value2", "column2")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe("( (column.Equals(@0))  OR  (column2.Equals(@1))  )");
        }
        
        [Fact]
        public void MultipleEqualWithOrAndStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column", SqlOperator.Equals, LogicalOperator.OR), WhereTerm.Default("value2", "column2"),
                WhereTerm.Default("value3", "column3")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(3);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe("( (column.Equals(@0))  OR  (column2.Equals(@1)) ) AND  (column3.Equals(@2))  ");
        }
        
        [Fact]
        public void MultipleNotEqualStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column", SqlOperator.NotEqual), WhereTerm.Default("value2", "column2", SqlOperator.NotEqual)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe(" (column.Equals(@0) == false)  AND  (column2.Equals(@1) == false)  ");
        }
        
        [Fact]
        public void MultipleBeginWithEndWithStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column", SqlOperator.BeginWith), WhereTerm.Default("value2", "column2", SqlOperator.EndWith)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe(" (column.StartsWith(@0))  AND  (column2.EndsWith(@1))  ");
        }
        
        [Fact]
        public void MultipleLikeAndIsNullStringParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column", SqlOperator.Like), WhereTerm.Default("value2", "column2", SqlOperator.IsNull)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains("value").ShouldBe(true);
            g.ListValue.Contains("value2").ShouldBe(true);
            result.ShouldBe(" (column.Contains(@0))  AND (column2 == null) ");
        }
        
        [Fact]
        public void MultipleEqualNumberParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default(1, "column"), WhereTerm.Default(2, "column2")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains(1).ShouldBe(true);
            g.ListValue.Contains(2).ShouldBe(true);
            result.ShouldBe(" (column = @0)  AND  (column2 = @1)  ");
        }
        
        [Fact]
        public void MultipleNotEqualNumberParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default(1, "column", SqlOperator.NotEqual), WhereTerm.Default(2, "column2", SqlOperator.GreatThan)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue.Contains(1).ShouldBe(true);
            g.ListValue.Contains(2).ShouldBe(true);
            result.ShouldBe(" (column <> @0)  AND  (column2 > @1)  ");
        }
        
        [Fact]
        public void MultipleGreatThenEqualLessThenLessThenEqualNumberParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default(1, "column", SqlOperator.GreatThanEqual), WhereTerm.Default(2, "column2", SqlOperator.LessThan), WhereTerm.Default(3, "column3", SqlOperator.LesThanEqual)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(3);
            g.ListValue.Contains(1).ShouldBe(true);
            g.ListValue.Contains(2).ShouldBe(true);
            result.ShouldBe(" (column >= @0)  AND  (column2 < @1)  AND  (column3 <= @2)  ");
        }
        
        [Fact]
        public void MultipleEqualDateParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default(DateTime.Now, "column"), WhereTerm.Default(DateTime.UtcNow, "column2")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(4);
            g.ListValue[0].ShouldBe(DateTime.Now.Date);
            g.ListValue[1].ShouldBe(DateTime.Now.AddDays(1).Date);
            g.ListValue[2].ShouldBe(DateTime.UtcNow.Date);
            g.ListValue[3].ShouldBe(DateTime.UtcNow.AddDays(1).Date);
            result.ShouldBe(" (column >= @0)   AND   (column < @1)  AND  (column2 >= @2)   AND   (column2 < @3)  ");
        }
        
        [Fact]
        public void MultipleGreatThenEqualLessThenLessThenEqualDateParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default(DateTime.Now, "column", SqlOperator.GreatThanEqual), 
                WhereTerm.Default(DateTime.Now, "column2", SqlOperator.LessThan), 
                WhereTerm.Default(DateTime.Now, "column3", SqlOperator.LesThanEqual),
                WhereTerm.Default(DateTime.Now, "column4", SqlOperator.GreatThan)
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(4);
            result.ShouldBe(" (column >= @0)  AND  (column2 < @1)  AND  (column3 <= @2)  AND  (column4 > @3)  ");
        }
        
        [Fact]
        public void DateRangeParameterShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTermDateTimeRange.Parameter(DateTime.Now.AddDays(-2), DateTime.Now, "column")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            result.ShouldBe(" (column >= @0  AND  column <= @1) ");
        }
        
        [Fact]
        public void NumberRangeParameterShouldBeSuccess()
        {
            var value1 = 10;
            var value2 = 20;
            var f = new ListParameter
            {
                WhereTermNumberRange.Parameter(value1, value2, "column")
            };
            var g  = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(2);
            g.ListValue[0].ShouldBe(value1);
            g.ListValue[1].ShouldBe(value2);
            result.ShouldBe(" (column >= @0  AND  column <= @1) ");
        }

        [Fact]
        public void EqualAndNotEqualsAndNullAndNotNullBoolParameterShouldBeSuccess()
        {
            bool value = true;
            bool value2 = false;
            var f = new ListParameter
            {
                WhereTerm.Default(value, "column"),
                WhereTerm.Default(value2, "column2", SqlOperator.NotEqual),
                WhereTerm.Default(null, "column3", SqlOperator.IsNull),
                WhereTerm.Default(null, "column4", SqlOperator.NotEqual)
            };
            var g = new LinqDynamicHelper();
            var result = g.GetQueryParameterLinq(f.ToArray());
            g.ListValue.Count.ShouldBe(4);
            g.ListValue[0].ShouldBe(value);
            g.ListValue[1].ShouldBe(value2);
            g.ListValue[2].ShouldBe(null);
            g.ListValue[3].ShouldBe(null);
            result.ShouldBe(" (column == @0)  AND  (column2 != @1)  AND (column3 == null) AND  (column4.Equals(@3) == false)  ");
        }
    }
}